#include "sga3_cl_basic_func.h"

/*
 * This lib-like file contains the necessary code to 
 * initialize the OpenCL environment.
 */

void buildKernel(cl_context context, cl_kernel *kernel, char *fileName, char* functionName, cl_int *err)
{
    FILE *kernelFile;
    size_t kernelSize;
    char *kernelBuffer;
    cl_program program;

    kernelFile = fopen(fileName, "r");
    if (kernelFile == (FILE *) NULL)
    {
        *err = FAIL_TO_OPEN_FILE;
        return;
    }
    fseek(kernelFile, 0, SEEK_END);
    kernelSize = ftell(kernelFile);
    rewind(kernelFile);
    kernelBuffer = (char *) malloc(kernelSize + 1);
    kernelBuffer[kernelSize] = '\0';
    fread(kernelBuffer, sizeof(char), kernelSize, kernelFile);
    fclose(kernelFile);
    program = clCreateProgramWithSource(context, 1, (const char**) &kernelBuffer, NULL, NULL);
    *err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (*err != CL_SUCCESS)
    {
        *err = FAIL_TO_BUILD_PROGRAM;
        return;
    }
    *kernel = clCreateKernel(program, functionName, NULL);
    if (!*kernel)
    {
        *err = FAIL_TO_CREATE_KERNEL;
        return;
    }
}

void printTextError(cl_int err)
{
    switch (err)
    {
    case FAIL_TO_FIND_DEVICE :
    {
        printf("Failed to find devices.\n");
        break;
    }
    case FAIL_TO_CREATE_CONTEXT :
    {
        printf("Failed to create context.\n");
        break;
    }
    case FAIL_TO_CREATE_COMMAND_QUEUE :
    {
        printf("Failed to create command queue.\n");
        break;
    }
    case FAIL_TO_BUILD_PROGRAM :
    {
        printf("Failed to build program.\n");
        break;
    }
    case FAIL_TO_CREATE_KERNEL :
    {
        printf("Failed to create kernel.\n");
        break;
    }
    case FAIL_TO_CREATE_MEMORY_OBJECT :
    {
        printf("Failed to create memory object.\n");
        break;
    }
    case FAIL_TO_SET_KERNEL_ARGS :
    {
        printf("Failed to set kernel arguments.\n");
        break;
    }
    case FAIL_TO_RUN_KERNEL :
    {
        printf("Failed to run kernel.\n");
        break;
    }
    case FAIL_TO_READ_BUFFER :
    {
        printf("Failed to read buffer.\n");
        break;
    }
    case FAIL_TO_OPEN_FILE :
    {
        printf("Failed to open file.\n");
        break;
    }
    case FAIL_TO_COPY_BUFFER :
    {
    	printf("Failed to copy buffer.\n");
    	break;
    }
    default: break;
    }
}

void initCLEnvironment(cl_context *context, cl_device_id *deviceIds,
                       cl_command_queue *commandQueue, cl_int *err)
{
    *err = clGetDeviceIDs(NULL, CL_DEVICE_TYPE_GPU, 1, deviceIds, NULL);
    if (*err != CL_SUCCESS)
    {
        *err = FAIL_TO_FIND_DEVICE;
        return;
    }

    *context = clCreateContext(0, 1, deviceIds, NULL, NULL, err);
    if (!*context)
    {
        *err = FAIL_TO_CREATE_CONTEXT;
        return;
    }

    *commandQueue = clCreateCommandQueue(*context, *deviceIds, CL_QUEUE_PROFILING_ENABLE, err);
    if (!*commandQueue)
    {
        *err = FAIL_TO_CREATE_COMMAND_QUEUE;
        return;
    }

}