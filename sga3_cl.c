#include "sga3_cl.h"

void initKernels(cl_context context, cl_kernel *wholeKernel,
                 cl_int *err)
{
    buildKernel(context, wholeKernel, WHOLE_KERNEL_NAME,
                WHOLE_KERNEL_FUNC, err);
}

void initMemBuffers(cl_context context,
                    cl_int _popsize, cl_int _nvar, cl_float *_rvar, cl_int _lchrom, cl_int _gen,
                    cl_float _pmutation, cl_uint _nmutation, cl_uint _ncross,
                    cl_float _pcross, cl_int _seed, cl_int _maxgen,
                    cl_mem *popsize, cl_mem *oldpop, cl_mem *newpop,
                    cl_mem *ncross, cl_mem *pcross,
                    cl_mem *nmutation, cl_mem *pmutation,
                    cl_mem *nvar, cl_mem *rvar, cl_mem *lchrom,
                    cl_mem *min, cl_mem *max, cl_mem *avg, cl_mem *gen,
                    cl_mem *max_ult, cl_mem *sumfitness, cl_mem *seed,
                    cl_mem *nmutationBuffer, cl_mem *ncrossBuffer, cl_mem *maxBuffer,
                    cl_mem *minBuffer, cl_mem *sumBuffer, cl_mem *rngBuffer_R, cl_mem *rngBuffer_N,
                    cl_mem *maxPopBuffer, cl_mem *maxgen, cl_int *err)
{
    *popsize = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                              sizeof(cl_int), &_popsize, NULL);

    *oldpop = clCreateBuffer(context, CL_MEM_READ_WRITE,
                             sizeof(ind_t) * Maxpop, NULL, NULL);

    *newpop = clCreateBuffer(context, CL_MEM_READ_WRITE,
                             sizeof(ind_t) * Maxpop, NULL, NULL);

    *ncross = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                             sizeof(cl_uint), &_ncross, NULL);

    *pcross = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                             sizeof(cl_float), &_pcross, NULL);

    *nmutation = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                sizeof(cl_uint), &_nmutation, NULL);

    *pmutation = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                                sizeof(cl_float), &_pmutation, NULL);

    *nvar = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                           sizeof(cl_int), &_nvar, NULL);

    *rvar = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                           sizeof(cl_float) * (2 * Nvar), _rvar, NULL);

    *lchrom = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                             sizeof(cl_int), &_lchrom, NULL);

    *min = clCreateBuffer(context, CL_MEM_READ_WRITE,
                          sizeof(cl_float), NULL, NULL);

    *max = clCreateBuffer(context, CL_MEM_READ_WRITE,
                          sizeof(cl_float), NULL, NULL);

    *avg = clCreateBuffer(context, CL_MEM_READ_WRITE,
                          sizeof(cl_float), NULL, NULL);

    *gen = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                          sizeof(cl_int), &_gen, NULL);

    *max_ult = clCreateBuffer(context, CL_MEM_READ_WRITE,
                              sizeof(max_t), NULL, NULL);

    *sumfitness = clCreateBuffer(context, CL_MEM_READ_WRITE,
                                 sizeof(cl_float), NULL, NULL);

    *nmutationBuffer = clCreateBuffer(context, CL_MEM_READ_WRITE,
                                      sizeof(cl_float) * (Maxpop / LOCAL_WORK_SIZE), NULL, NULL);

    *ncrossBuffer = clCreateBuffer(context, CL_MEM_READ_WRITE,
                                   sizeof(cl_float) * (Maxpop / LOCAL_WORK_SIZE), NULL, NULL);

    *maxBuffer = clCreateBuffer(context, CL_MEM_READ_WRITE,
                                sizeof(cl_float) * (Maxpop / LOCAL_WORK_SIZE), NULL, NULL);

    *minBuffer = clCreateBuffer(context, CL_MEM_READ_WRITE,
                                sizeof(cl_float) * (Maxpop / LOCAL_WORK_SIZE), NULL, NULL);

    *sumBuffer = clCreateBuffer(context, CL_MEM_READ_WRITE,
                                sizeof(cl_float) * (Maxpop / LOCAL_WORK_SIZE), NULL, NULL);

    *seed = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                           sizeof(cl_int), &_seed, NULL);

    *rngBuffer_R = clCreateBuffer(context, CL_MEM_READ_WRITE,
                                  sizeof(cl_uint) * (1000 * Maxpop), NULL, NULL);

    *rngBuffer_N = clCreateBuffer(context, CL_MEM_READ_WRITE,
                                  sizeof(cl_int) * (Maxpop), NULL, NULL);

    *maxPopBuffer = clCreateBuffer(context, CL_MEM_READ_WRITE,
                                   sizeof(max_t) * (Maxpop / LOCAL_WORK_SIZE), NULL, NULL);

    *maxgen = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                           sizeof(cl_int), &_maxgen, NULL);

    if (!*popsize || !*oldpop || !*newpop || !*ncross || !*pcross ||
            !*nmutation || !*pmutation || !*nvar || !*rvar || !*lchrom ||
            !*min || !*max || !*avg || !*gen || !*max_ult || !*sumfitness ||
            !*nmutationBuffer || !*ncrossBuffer || !*maxBuffer || !*minBuffer ||
            !*sumBuffer || !*rngBuffer_R || !*rngBuffer_N || !*maxPopBuffer || !*maxgen)
    {
        *err = FAIL_TO_CREATE_MEMORY_OBJECT;
        return;
    }
}

void initdata(cl_int *popsize, cl_int *lchrom, cl_int *maxgen, cl_float *pcross,
              cl_float *pmutation, cl_uint *nmutation, cl_uint *ncross, cl_int *nvar,
              cl_float *rvar, cl_int *seed)
{
    register int i, j;
    FILE *fp;
    char buf[120], dummy[50];

    if ((fp = fopen("sga3.var", "r")) == (FILE *)NULL)
    {printf("something wrong with sga3.var\n"); exit(1);}


    fgets(buf, 120, fp); sscanf(buf, "%s %d", dummy, popsize);

    fgets(buf, 120, fp); sscanf(buf, "%s %d", dummy, lchrom);

    fgets(buf, 120, fp); sscanf(buf, "%s %d", dummy, maxgen);

    fgets(buf, 120, fp); sscanf(buf, "%s %f", dummy, pcross);

    fgets(buf, 120, fp); sscanf(buf, "%s %f", dummy, pmutation);

    fgets(buf, 120, fp); sscanf(buf, "%s %d", dummy, seed);

    fgets(buf, 120, fp); sscanf(buf, "%s %d", dummy, nvar);

    for (i = 1; i <= *nvar; i++)
    {
        j = 2 * i - 1;
        fgets(buf, 120, fp);
        sscanf(buf, "%s %f %s %f", dummy, &rvar[j], dummy, &rvar[j + 1]);
    }

    *nmutation = 0;

    *ncross = 0;

    fclose(fp);
}

void readMemBufferForReport(cl_command_queue commandQueue,
                            ind_t *_oldpop, ind_t *_newpop, cl_float *_max, cl_float *_avg,
                            cl_float *_min, cl_float *_sumfitness, cl_uint *_nmutation,
                            cl_uint *_ncross, max_t *_max_ult,
                            cl_mem *oldpop, cl_mem *newpop, cl_mem *max, cl_mem *avg,
                            cl_mem *min, cl_mem *sumfitness, cl_mem *nmutation,
                            cl_mem *ncross, cl_mem *max_ult,
                            cl_int _popsize, cl_int *err)
{
    if (_oldpop != NULL)
    {
        *err |= clEnqueueReadBuffer(commandQueue, *oldpop, CL_TRUE, 0,
                                    (Maxpop) * sizeof(ind_t), _oldpop, 0, NULL, NULL);
    }
    if (_newpop != NULL)
    {
        *err |= clEnqueueReadBuffer(commandQueue, *newpop, CL_TRUE, 0,
                                    (Maxpop) * sizeof(ind_t), _newpop, 0, NULL, NULL);
    }
    if (_max != NULL)
    {
        *err |= clEnqueueReadBuffer(commandQueue, *max, CL_TRUE, 0,
                                    sizeof(cl_float), _max, 0, NULL, NULL);
    }
    if (_avg != NULL)
    {
        *err |= clEnqueueReadBuffer(commandQueue, *avg, CL_TRUE, 0,
                                    sizeof(cl_float), _avg, 0, NULL, NULL);
    }
    if (_min != NULL)
    {
        *err |= clEnqueueReadBuffer(commandQueue, *min, CL_TRUE, 0,
                                    sizeof(cl_float), _min, 0, NULL, NULL);
    }
    if (_sumfitness != NULL)
    {
        *err |= clEnqueueReadBuffer(commandQueue, *sumfitness, CL_TRUE, 0,
                                    sizeof(cl_float), _sumfitness, 0, NULL, NULL);
    }
    if (_nmutation != NULL)
    {
        *err |= clEnqueueReadBuffer(commandQueue, *nmutation, CL_TRUE, 0,
                                    sizeof(cl_uint), _nmutation, 0, NULL, NULL);
    }
    if (_ncross != NULL)
    {
        *err |= clEnqueueReadBuffer(commandQueue, *ncross, CL_TRUE, 0,
                                    sizeof(cl_uint), _ncross, 0, NULL, NULL);
    }
    if (_max_ult != NULL)
    {
        *err |= clEnqueueReadBuffer(commandQueue, *max_ult, CL_TRUE, 0,
                                    sizeof(max_t), _max_ult, 0, NULL, NULL);
    }
    if (*err != CL_SUCCESS)
    {
        *err = FAIL_TO_READ_BUFFER;
        return;
    }
}

int getGlobalWorkSize(int popsize)
{
    int whole = popsize % LOCAL_WORK_SIZE;
    if (whole != 0)
        return ((int) (popsize / LOCAL_WORK_SIZE) + 1) * LOCAL_WORK_SIZE;
    else
        return ((int) (popsize / LOCAL_WORK_SIZE)) * LOCAL_WORK_SIZE;
}

void cl_Run(cl_command_queue *commandQueue, cl_kernel *kernel,
            cl_mem *popsize, cl_mem *gen, cl_mem *oldpop,
            cl_mem *newpop, cl_mem *max, cl_mem *avg, cl_mem *min,
            cl_mem *sumfitness, cl_mem *max_ult,
            cl_mem *nmutation, cl_mem *ncross, cl_mem *pmutation, cl_mem *pcross,
            cl_mem *nvar, cl_mem *rvar, cl_mem *lchrom, cl_mem *seed,
            cl_mem *nmutationBuffer, cl_mem *ncrossBuffer, cl_mem *maxBuffer,
            cl_mem *minBuffer, cl_mem *sumBuffer, cl_mem *rngBuffer_R, cl_mem *rngBuffer_N,
            cl_mem *maxPopBuffer, cl_mem *maxgen, cl_int _popsize, cl_int *err)
{
    size_t global_work_size, local_work_size;
    int i = 0;

    *err  = clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)oldpop);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)newpop);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)popsize);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)max);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)min);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)avg);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)sumfitness);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)max_ult);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)nmutation);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)ncross);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)pmutation);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)pcross);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)nvar);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)rvar);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)lchrom);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)seed);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)nmutationBuffer);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)ncrossBuffer);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)maxBuffer);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)minBuffer);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)sumBuffer);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)rngBuffer_R);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)rngBuffer_N);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)maxPopBuffer);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)gen);
    *err |= clSetKernelArg(*kernel, i++, sizeof(cl_mem), (void *)maxgen);

    if (*err != CL_SUCCESS)
    {
        printf("[CL_RUN] ");
        *err = FAIL_TO_SET_KERNEL_ARGS;
        return;
    }

    global_work_size = getGlobalWorkSize(_popsize);
    local_work_size = LOCAL_WORK_SIZE;
    cl_event e;
    *err = clEnqueueNDRangeKernel(*commandQueue, *kernel, 1, NULL,
                                  &global_work_size, &local_work_size, 0, NULL, &e);
    if (*err != CL_SUCCESS)
    {
        printf("[CL_RUN] ");
        *err = FAIL_TO_RUN_KERNEL;
        return;
    }
    clFinish(*commandQueue);
    /*cl_ulong time_start, time_end;
    double total_time;
    clGetEventProfilingInfo(e, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
    clGetEventProfilingInfo(e, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
    total_time = time_end - time_start;
    printf("[CL_RUN] Execution time in milliseconds = %0.3f ms\n", (total_time / 1000000.0) );*/

}

int main(int argc, char *argv[])
{
    // Local variables
    cl_int _popsize, _nvar, _lchrom, _maxgen, _gen = 0, err = 0, _seed;
    cl_uint _nmutation, _ncross;
    cl_float _rvar[2 * Nvar], _pmutation, _pcross;

    // Variables for report
    cl_float _max, _avg, _min, _sumfitness;
    max_t _max_ult;
    ind_t _oldpop[Maxpop], _newpop[Maxpop];

    // CL Variables
    cl_context context;
    cl_device_id deviceIds;
    cl_command_queue commandQueue;
    cl_kernel wholeKernel;
    cl_mem popsize, oldpop, newpop, ncross, pcross,
           nmutation, pmutation, nvar, rvar, lchrom,
           min, max, avg, gen, max_ult, sumfitness, seed,
           nmutationBuffer, ncrossBuffer, maxBuffer,
           minBuffer, sumBuffer, rngBuffer_R, rngBuffer_N, maxPopBuffer, maxgen;

    time_t t_start, t_finish, t_diff;

    initdata(&_popsize, &_lchrom, &_maxgen, &_pcross, &_pmutation, &_nmutation, &_ncross,
             &_nvar, _rvar, &_seed);

    initCLEnvironment(&context, &deviceIds, &commandQueue, &err);
    REPORTERR(err)

    initMemBuffers(context, _popsize,  _nvar,  _rvar,  _lchrom, _gen, _pmutation,
                     _nmutation, _ncross, _pcross, _seed, _maxgen, &popsize, &oldpop,
                     &newpop, &ncross, &pcross, &nmutation, &pmutation,
                     &nvar, &rvar, &lchrom, &min, &max, &avg, &gen,
                     &max_ult, &sumfitness, &seed, &nmutationBuffer, &ncrossBuffer,
                     &maxBuffer, &minBuffer, &sumBuffer, &rngBuffer_R, &rngBuffer_N, &maxPopBuffer, &maxgen, &err);
    REPORTERR(err)


    initKernels(context, &wholeKernel, &err);
    REPORTERR(err)



    time(&t_start);

    do
    {
        if (_gen > 1) 
        {
        	if(_gen + 1000 < _maxgen + 1)
        		_gen += 1000;
        	else
        		_gen = _maxgen + 1;
        }

        printf("n.gen = %d", _gen);
        printf("\n\033[F\033[J");

        cl_Run(&commandQueue, &wholeKernel, &popsize, &gen, &oldpop,
               &newpop, &max, &avg, &min, &sumfitness, &max_ult,
               &nmutation, &ncross, &pmutation, &pcross,
               &nvar, &rvar, &lchrom, &seed,
               &nmutationBuffer, &ncrossBuffer, &maxBuffer,
               &minBuffer, &sumBuffer, &rngBuffer_R, &rngBuffer_N,
               &maxPopBuffer, &maxgen, _popsize, &err);
        REPORTERR(err)


        if (_gen == 0 || _gen == 1 || _gen == _maxgen + 1)
        {
            readMemBufferForReport(commandQueue,
                                   _oldpop, _newpop, &_max, &_avg,
                                   &_min, &_sumfitness, &_nmutation,
                                   &_ncross, &_max_ult,
                                   &oldpop, &newpop, &max, &avg,
                                   &min, &sumfitness, &nmutation,
                                   &ncross, &max_ult,
                                   _popsize, &err);
            REPORTERR(err)


            if (_gen == 0)
            {
                initreport(_popsize, _lchrom, _maxgen, _pcross, _pmutation,
                           _max, _avg, _min, _sumfitness);
            }
            else
            {
                report(_gen, _oldpop, _newpop, _lchrom, _max, _avg, _min, _sumfitness,
                       _nmutation, _ncross, _popsize, _nvar, &_max_ult);
            }
        }

        if (_gen < 2) _gen++;
        //if(_gen == 2)break;
        //if(_gen == 1000)break;
    } while (_gen <= _maxgen);

    time(&t_finish);  t_diff = t_finish - t_start;

    printf("Total Time (mins.) = %f\n", (float) t_diff / 60.);

    printf("The results are stored in ***genout.dat***\n");

    return 0;
}