#include "sga3_report.h"

/*
 * To make the code more clear, these report related functions
 * are moved here.
 */

void report(cl_int gen, ind_t *oldpop, ind_t *newpop,
            cl_int lchrom, cl_float max, cl_float avg, cl_float min, cl_float sumfitness,
            cl_uint nmutation, cl_uint ncross, cl_int popsize,
            cl_int nvar, max_t *max_ult)
{
    register int k;
    FILE *fpout;
    int j;
    float sum;

    if ((fpout = fopen("genout.dat", "a")) == (FILE *) NULL)
    {printf("cannot open genout.dat\n"); exit(1);}

    fprintf(fpout, "Generation #%5d data (lchrom %5d)\n", gen, lchrom);
    fprintf(fpout, "No. parent1 parent2      x     fitness\n");
    fprintf(fpout, "----------------------------------\n");

    fprintf(fpout, "Old Generation of Generation No. %5d\n", gen);
    for (j = 0; j <= popsize; j++)
    {
        fprintf(fpout, "%5d %5d %5d", j, oldpop[j].parent1, oldpop[j].parent2);
        for (k = 0; k < nvar; k++)
            fprintf(fpout, " %9.3e", oldpop[j].x[k]);
        fprintf(fpout, "    %9.3e\n", oldpop[j].objective);
    }

    fprintf(fpout, "New Generation of Generation No. %5d\n", gen);
    for (j = 0; j <= popsize; j++)
    {
        fprintf(fpout, "%5d %5d %5d", j, newpop[j].parent1, newpop[j].parent2);
        for (k = 0; k < nvar; k++)
            fprintf(fpout, " %9.3e", newpop[j].x[k]);
        fprintf(fpout, "    %9.3e\n", newpop[j].objective);
    }

    fprintf(fpout, "New (%5d) generation's statistics\n", gen);

    fprintf(fpout, " max=%10.5e avg=%10.5e min=%10.5e sum=%10.5e\n nmutation=%lu \
ncross=%lu\n", max, avg, min, sumfitness, nmutation, ncross);

    fprintf(fpout, "\nMax of all generations\n");
    fprintf(fpout, "occurred in generation no.%5d \n", max_ult->gen);
    fprintf(fpout, "chromosome variables\n");
    for (k = 0; k < nvar; k++)
        fprintf(fpout, " %5d", max_ult->chrom[k]);
    fprintf(fpout, "\n");
    fprintf(fpout, "design variables\n");
    for (k = 0; k < nvar; k++)
        fprintf(fpout, " %9.3e", max_ult->x[k]);
    fprintf(fpout, "\n");
    fprintf(fpout, "raw fitness = %f\n", max_ult->objective);
    fclose(fpout);
}

void initreport(cl_int popsize, cl_int lchrom, cl_int maxgen, cl_float pcross,
                cl_float pmutation, cl_float max, cl_float avg, cl_float min,
                cl_float sumfitness)
{
    FILE *fpout;

    if ((fpout = fopen("genout.dat", "w")) == (FILE *) NULL)
    {printf("cannot open genout.dat\n"); exit(1);}

    fprintf(fpout, "Population size (popsize) = %5d\n", popsize);
    fprintf(fpout, "Chromosome length (lchrom) = %5d\n", lchrom);
    fprintf(fpout, "Maximum # of generations (maxgen) %5d\n", maxgen);
    fprintf(fpout, "Crossover probability (pcross) = %10.5e\n", pcross);
    fprintf(fpout, "Mutation probability (pmutation) = %10.5e\n", pmutation);
    fprintf(fpout, "\n Initial Generation Statistics\n");
    fprintf(fpout, "---------------------------------\n");
    fprintf(fpout, "\n");
    fprintf(fpout, "Initial population maximum fitness = %10.5e\n", max);
    fprintf(fpout, "Initial population average fitness = %10.5e\n", avg);
    fprintf(fpout, "Initial population minimum fitness = %10.5e\n", min);
    fprintf(fpout, "Initial population sum of  fitness = %10.5e\n", sumfitness);
    fprintf(fpout, "\n\n\n");
    fclose(fpout);
}