#include "wholeKernel.clh"

/*
 * This is the kernel of sga3
 * This kernel contains both initpop and the looping functions
 * such as statistic, generation and scalepop etc.
 */

float objfunc(float *x)
{
    float result, penalty;

    result = 700 - (x[1] + pow(x[2], 2) + x[2] * x[3]);

    penalty = 1000 * pow(x[1] + x[2] + x[3], 2);

    result -= penalty;

    if (fabs(result) <= (float) 0) result = Tol;

    return result;
}

void prescale(float umax, float uavg, float umin, __local float *a, __local float *b)
{
    float delta;

    if (umin > (Fmultiple * uavg - umax) / (Fmultiple - 1.)) /*Non-neg test*/
    {
        delta = umax - uavg;
        if (delta == 0.) delta = .00000001;
        *a = (Fmultiple - 1.) * uavg / delta;
        *b = uavg * (umax - Fmultiple * uavg) / delta;
    }
    else
    {
        delta = uavg - umin;
        if (delta == 0.) delta = .00000001;
        *a = uavg / delta;
        *b = -umin * uavg / delta;
    }
}

float chrom_map_x(int chrom, float range1, float range2, int lchrom)
{
    float diff, add_percent, res;

    diff = range2 - range1;
    add_percent = ((float) chrom / (pow(2., (float)lchrom) - 1.)) * diff;
    res = range1 + add_percent;

    return (res);
}


float random1(uint *r, int *n)
{
    float result;
    result = (float) rand(r, n) / RAND_MAX;
    return (result);
}

int flip(float probability, uint *r, int *n)
{
    float random1_t;
    if (probability == 1.0) return (1);

    random1_t = random1(r, n);

    if (random1_t <= probability) return (1);
    else return (0);
}



void bit_on_off(int *chrom, int i)
{
    switch (i) {
    case 1:  *chrom ^= 01; break;  /*set on the 1st bit */
    case 2:  *chrom ^= 02; break;
    case 3:  *chrom ^= 04; break;
    case 4:  *chrom ^= 010; break;
    case 5:  *chrom ^= 020; break;
    case 6:  *chrom ^= 040; break;
    case 7:  *chrom ^= 0100; break;
    case 8:  *chrom ^= 0200; break;
    case 9:  *chrom ^= 0400; break;
    case 10:  *chrom ^= 01000; break;
    case 11: *chrom ^= 02000; break;
    case 12: *chrom ^= 04000; break;
    case 13: *chrom ^= 010000; break;
    case 14: *chrom ^= 020000; break;
    case 15: *chrom ^= 040000; break;
    case 16: *chrom ^= 0100000; break;
    case 17: *chrom ^= 0200000; break;
    case 18: *chrom ^= 0400000; break;
    case 19: *chrom ^= 01000000; break;
    case 20: *chrom ^= 02000000; break;
    case 21: *chrom ^= 04000000; break;
    case 22: *chrom ^= 010000000; break;
    case 23: *chrom ^= 020000000; break;
    case 24: *chrom ^= 040000000; break;
    case 25: *chrom ^= 0100000000; break;
    case 26: *chrom ^= 0200000000; break;
    case 27: *chrom ^= 0400000000; break;
    case 28: *chrom ^= 01000000000; break;
    case 29: *chrom ^= 02000000000; break;
    case 30: *chrom ^= 04000000000; break;
    case 31: *chrom ^= 010000000000; break;
    }
}

void bit_on(int *chrom, int i)
{
    switch (i) {
    case 1:  *chrom |= 01; break;  /*set on the 1st bit */
    case 2:  *chrom |= 02; break;
    case 3:  *chrom |= 04; break;
    case 4:  *chrom |= 010; break;
    case 5:  *chrom |= 020; break;
    case 6:  *chrom |= 040; break;
    case 7:  *chrom |= 0100; break;
    case 8:  *chrom |= 0200; break;
    case 9:  *chrom |= 0400; break;
    case 10: *chrom |= 01000; break;
    case 11: *chrom |= 02000; break;
    case 12: *chrom |= 04000; break;
    case 13: *chrom |= 010000; break;
    case 14: *chrom |= 020000; break;
    case 15: *chrom |= 040000; break;
    case 16: *chrom |= 0100000; break;
    case 17: *chrom |= 0200000; break;
    case 18: *chrom |= 0400000; break;
    case 19: *chrom |= 01000000; break;
    case 20: *chrom |= 02000000; break;
    case 21: *chrom |= 04000000; break;
    case 22: *chrom |= 010000000; break;
    case 23: *chrom |= 020000000; break;
    case 24: *chrom |= 040000000; break;
    case 25: *chrom |= 0100000000; break;
    case 26: *chrom |= 0200000000; break;
    case 27: *chrom |= 0400000000; break;
    case 28: *chrom |= 01000000000; break;
    case 29: *chrom |= 02000000000; break;
    case 30: *chrom |= 04000000000; break;
    case 31: *chrom |= 010000000000; break;
    }
}


int  bit_check(int chrom, int i)
{
    switch (i) {
    case 1:  if (chrom & 01)return (1); break;        /*if bit was on return [1]*/
    case 2:  if (chrom & 02)return (1); break;
    case 3:  if (chrom & 04)return (1); break;
    case 4:  if (chrom & 010)return (1); break;
    case 5:  if (chrom & 020)return (1); break;
    case 6:  if (chrom & 040)return (1); break;
    case 7:  if (chrom & 0100)return (1); break;
    case 8:  if (chrom & 0200)return (1); break;
    case 9:  if (chrom & 0400)return (1); break;
    case 10:  if (chrom & 01000)return (1); break;
    case 11: if (chrom & 02000)return (1); break;
    case 12: if (chrom & 04000)return (1); break;
    case 13: if (chrom & 010000)return (1); break;
    case 14: if (chrom & 020000)return (1); break;
    case 15: if (chrom & 040000)return (1); break;
    case 16: if (chrom & 0100000)return (1); break;
    case 17: if (chrom & 0200000)return (1); break;
    case 18: if (chrom & 0400000)return (1); break;
    case 19: if (chrom & 01000000)return (1); break;
    case 20: if (chrom & 02000000)return (1); break;
    case 21: if (chrom & 04000000)return (1); break;
    case 22: if (chrom & 010000000)return (1); break;
    case 23: if (chrom & 020000000)return (1); break;
    case 24: if (chrom & 040000000)return (1); break;
    case 25: if (chrom & 0100000000)return (1); break;
    case 26: if (chrom & 0200000000)return (1); break;
    case 27: if (chrom & 0400000000)return (1); break;
    case 28: if (chrom & 01000000000)return (1); break;
    case 29: if (chrom & 02000000000)return (1); break;
    case 30: if (chrom & 04000000000)return (1); break;
    case 31: if (chrom & 010000000000)return (1); break;
    }
    return (0);                              /* if bit is no on then return [0] */
}

int round_(int j, int lchrom)
{

    return (fmod((float)j, (float)lchrom) == 0) ? 1 : 0;
}

int  rnd(int low, int high, uint *r, int *n)
{
    int i;

    if (low >= high) i = low;
    else
    {
        i = (int) (random1(r, n) * (high - low + 1) + low);
        if (i > high) i = high;
    }
    return i;
}

int  mutation(float pmutation, unsigned int *nmutation, uint *r, int *n)
{
    int mutate;

    mutate = flip(pmutation, r, n);
    if (mutate == 1)
    {
        *nmutation += 1;
        return (1);
    }
    else
        return (0);
}

// For this function, if lid is odd, should pass in parent1
// If lid is even, pass in parent2
void crossover(int *parent, int *child,
               int nvar, int lchrom, unsigned int *nmutation,
               int jcross, float pmutation,
               uint *r, int *n)
{
    int k, kk, j, lighted, test, rn;
    rn = 0;                                                   /*chrom counter*/
    kk = 0;
    int flag = 0;
    for (j = 1; j <= jcross; j++)
    {

        if (rn == 1) kk++;

        rn = round_(j, lchrom);

        k = j - kk * lchrom;
        test = mutation(pmutation, nmutation, r, n);
        if (test == 1)bit_on_off(&child[kk], k);                     /* mutation */
        k++;
    }

    if (jcross != nvar * lchrom)
    {
        for (j = jcross + 1; j <= nvar * lchrom; j++)
        {
            if (rn == 1) kk++;
            rn = round_(j, lchrom);
            k = j - kk * lchrom;
            lighted = bit_check(parent[kk], k);       /*lighted = [1] if bit is on */
            test = mutation(pmutation, nmutation, r, n);
            /*test = [0] no change , test = 1 bit changed jth bit is altered*/
            bit_on(&child[kk], k);
            if (lighted == 0) bit_on_off(&child[kk], k);
            if (test == 1)bit_on_off(&child[kk], k);  /* mutate */
        }
    }
}

int select1(int *nremain, __local int *choices, uint *r, int *n)
{
    int jpick, index;

    jpick = rnd(1, *nremain, r, n);

    index = choices[jpick];

    choices[jpick] = choices[*nremain];

    *nremain = *nremain - 1;

    return (index);
}

void storeRNGState(__global uint *gr, __global int *gn,
                   uint *r, int *n, int gid)
{
    int k = 0;
    for (k = 0; k < 1000; k++)
        gr[k + gid * 1000] = r[k];
    gn[gid] = *n;
}

void loadRNGState(__global uint *gr, __global int *gn,
                  uint *r, int *n, int gid)
{
    int k = 0;
    for (k = 0; k < 1000; k++)
        r[k] = gr[k + gid * 1000];
    *n = gn[gid];
}

__kernel void run(__global pop_t *globalOldPop, __global pop_t *globalNewPop,
                  __constant int *popsize,
                  __global float *globalMax, __global float *globalMin,
                  __global float *globalAvg, __global float *globalSumfitness,
                  __global max_t *globalMaxPop,
                  __global int *globalNmutation, __global int *globalNcross,
                  __constant float *pmutation, __constant float *pcross,
                  __constant int *nvar, __constant float *rvar,
                  __constant int *lchrom, __constant int *seed,
                  __global uint *nmutationBuffer, __global uint *ncrossBuffer,
                  __global float *maxBuffer, __global float *minBuffer, __global float *sumBuffer,
                  __global uint *rngBuffer_R, __global int *rngBuffer_N,
                  __global max_t *maxPopBuffer, __global int *gen, __global int *maxgen)
{
    // These const variables are not defined in __constant
    // They are defined in __private
    // We can not use too many __constant mem or
    // the concurrent threads will be limited.
    const int gid = get_global_id(0);
    const int globalSize = get_global_size(0);
    const int groupId = get_group_id(0);
    const int lid = get_local_id(0);
    int nextNeighbour, lastNeighbour;
    int myGlobalPopSize = *popsize;
    const int isOdd = lid % 2;
    int loopId = 0;
    __local pop_t localOldPop[LOCAL_SIZE], localNewPop[LOCAL_SIZE];
    localOldPop[lid] = globalOldPop[lid + groupId * LOCAL_SIZE];
    __local max_t localMaxPop;
    if (gid == 0)localMaxPop = *globalMaxPop;
    __local float localMax, localMin, localAvg, localSumfitness;
    // Variables for random number generator
    uint realR[1000];
    int n;
    if (*gen == 0)
        srand(realR, &n, 3UL);
    else
    {
        loadRNGState(rngBuffer_R, rngBuffer_N, realR, &n, gid);
    }
    // Scale pop needed variables
    __local float a, b;
    // Generation needed variables
    __local int k;
    int j, myK, myKK, jassign;
    __local int choices[LOCAL_SIZE], mate[LOCAL_SIZE];
    float fraction, expected, temp;
    float myPcross = *pcross, myPmutation = *pmutation;
    __local uint localNmutation, localNcross;
    uint myNmutation = 0;
    __local int jcross[LOCAL_SIZE];
    int myJcross, myLchrom = *lchrom, myNvar = *nvar;
    __local int localMyNmutation[LOCAL_SIZE], localMyNcross[LOCAL_SIZE];


    // Normally, read and write to mypop, this is faster
    // Only when doing work group size reduction, we copy
    // private mypop to local localpop
    // Only when a global reduction is needed, we copy the value
    // into global oldpop or newpop



    localMax = *globalMax;
    localMin = *globalMin;
    localAvg = *globalAvg;
    localSumfitness = *globalSumfitness;
    __local float fractionMax[LOCAL_SIZE], fractionMin[LOCAL_SIZE],
            fractionSum[LOCAL_SIZE];

    pop_t myParentPop, myNewPop, myOldPop;
    int offset;




    // Initialise the neighbour relation
    if (lid == LOCAL_SIZE - 1)
    {
        nextNeighbour = 0;
        lastNeighbour = lid - 1;
    }
    else if (lid == 0)
    {
        nextNeighbour = lid + 1;
        lastNeighbour = LOCAL_SIZE - 1;
    }
    else
    {
        nextNeighbour = lid + 1;
        lastNeighbour = lid - 1;
    }

    // Load local pop to private pop
    myOldPop = localOldPop[lid];


    if (*gen == 0)
    {
        /*
        *	Initpop
        */
        //int l;
        for (int j = 0; j < lid + groupId * LOCAL_SIZE; j++)
        	rand(realR, &n);
        for (j = 0; j < rand(realR, &n) % 7; j++)
        	rand(realR, &n);
        for (myK = 0; myK < myNvar; myK++)
        {
            for (j = 1; j <= myLchrom; j++)
            {
                if (flip(0.5, realR, &n) > 0) bit_on(&(myOldPop.chrom[myK]), j);
            }
            
            myKK = 2 * myK;
            myOldPop.x[myK] = chrom_map_x(myOldPop.chrom[myK], rvar[myKK], rvar[myKK + 1], myLchrom);
        }
        myOldPop.objective = objfunc(myOldPop.x);
        myOldPop.parent1 = myOldPop.parent2 = myOldPop.xsite = 0;
        globalOldPop[gid] = localOldPop[lid] = myOldPop;
        fractionMax[lid] = fractionMin[lid] = fractionSum[lid] = myOldPop.objective;

        barrier(CLK_LOCAL_MEM_FENCE);

        for (offset = LOCAL_SIZE / 2; offset > 0; offset >>= 1)
        {
            if (lid < offset)
            {
                float self = fractionMax[lid];
                float other = fractionMax[lid + offset];
                fractionMax[lid] = max(self, other);
                self = fractionMin[lid];
                other = fractionMin[lid + offset];
                fractionMin[lid] = min(self, other);
                self = fractionSum[lid];
                other = fractionSum[lid + offset];
                fractionSum[lid] = self + other;
            }
            barrier(CLK_LOCAL_MEM_FENCE);
        }

        if (lid == 0)
        {
            localMax = fractionMax[0];
            localMin = fractionMin[0];
            localSumfitness = fractionSum[0];
            maxBuffer[groupId] = localMax;
            minBuffer[groupId] = localMin;
            sumBuffer[groupId] = localSumfitness;
            
        }
        barrier(CLK_GLOBAL_MEM_FENCE);
        if (gid == 0)
        {
            *globalMax = *globalMin = *globalSumfitness = *globalAvg = 0;
            for (j = 0; j < globalSize / LOCAL_SIZE; j++)
            {
                *globalMax = max(maxBuffer[j], *globalMax);
                *globalMin = min(minBuffer[j], *globalMin);
                *globalSumfitness += sumBuffer[j];
            }
            *globalAvg = *globalSumfitness / globalSize;
        }
        if (gid == 0) *gen += 1;
        storeRNGState(rngBuffer_R, rngBuffer_N, realR, &n, gid);
        return;
    }

    if (*gen != 0) {
        int loopMax;
        if (*gen == 1) loopMax = 1;
        else loopMax = 1000;

        for (loopId = 0; loopId < loopMax; loopId++)
        {
            /*
             *  Scale population
             */
            if (lid == 0)
            {
                a = b = localSumfitness = 0;
                prescale(localMax, localAvg, localMin, &a, &b);
            }

            barrier(CLK_LOCAL_MEM_FENCE);
            myOldPop.fitness = a * myOldPop.objective + b;
            fractionSum[lid] = myOldPop.fitness;
            barrier(CLK_LOCAL_MEM_FENCE);
            for (offset = (LOCAL_SIZE - 1) / 2; offset > 0; offset >>= 1)
            {
                if (lid < offset)
                {
                    fractionSum[lid] = fractionSum[lid] + fractionSum[lid + offset];
                }
                barrier(CLK_LOCAL_MEM_FENCE);
            }
            if (lid == 0)
            {
                localSumfitness = fractionSum[0];
            }
            // Optional barrier?
            barrier(CLK_LOCAL_MEM_FENCE);
            /*
            	Start of generation
            */
            if (lid == 0) k = 0;
            expected = myOldPop.fitness / (localAvg + 1.e-30);
            fraction = modf(expected, &temp);
            jassign = (int) temp;
            while (jassign > 0)
            {
                myK = atomic_inc(&k) + 1;

                jassign--;
                choices[myK] = lid;
            }
            while (k < LOCAL_SIZE)
            {
                if (fraction > 0)
                {
                    if (flip(fraction, realR, &n) == 1)
                    {
                        myK = atomic_inc(&k) + 1;
                        if (myK > LOCAL_SIZE) break;
                        choices[myK] = lid;
                    }
                }
            }
            if (lid == 0)
            {
                int c, nremain = LOCAL_SIZE;
                for (c = 0; c < LOCAL_SIZE; c++)mate[c] = select1(&nremain, choices, realR, &n);
            }
            barrier(CLK_LOCAL_MEM_FENCE);
            choices[lid] = mate[lid];
            jcross[lid] = 0;
            if (isOdd)
            {
                myParentPop = localOldPop[choices[nextNeighbour]];
                myNewPop = localOldPop[choices[lid]];

                if (flip(myPcross, realR, &n) == 1)
                {
                    jcross[nextNeighbour] = jcross[lid] = rnd(1, myNvar * myLchrom - 1, realR, &n);
                    localMyNcross[lid]++;
                }
                else
                    jcross[nextNeighbour] = jcross[lid] = myNvar * myLchrom;
            }
            else
            {
                myParentPop = localOldPop[choices[lastNeighbour]];
                myNewPop = localOldPop[choices[lid]];
            }

            barrier(CLK_LOCAL_MEM_FENCE);

            myJcross = jcross[lid];
            crossover(myParentPop.chrom, myNewPop.chrom, myNvar, myLchrom, &myNmutation,
                      myJcross, myPmutation, realR, &n);

            localMyNmutation[lid] = myNmutation;

            for (myK = 0; myK <= myNvar; myK++)
            {
                myKK = 2 * myK;
                myNewPop.x[myK] = chrom_map_x(myNewPop.chrom[myK], rvar[myKK], rvar[myKK + 1], myLchrom);
            }

            myNewPop.objective = objfunc(myNewPop.x);
            myNewPop.xsite = myJcross;
            if (isOdd)
            {
                myNewPop.parent1 = choices[lastNeighbour];
                myNewPop.parent2 = choices[lid];

            }
            else
            {
                myNewPop.parent1 = choices[lid];
                myNewPop.parent2 = choices[nextNeighbour];
            }
            localOldPop[lid] = myNewPop;

            /*
             *	Start of statistic
             */

            fractionMax[lid] = fractionMin[lid] = fractionSum[lid] = myNewPop.objective;

            barrier(CLK_LOCAL_MEM_FENCE);

            for (offset = LOCAL_SIZE / 2; offset > 0; offset >>= 1)
            {
                if (lid < offset)
                {
                    float self = fractionMax[lid];
                    float other = fractionMax[lid + offset];
                    fractionMax[lid] = max(self, other);
                    self = fractionMin[lid];
                    other = fractionMin[lid + offset];
                    fractionMin[lid] = min(self, other);
                    self = fractionSum[lid];
                    other = fractionSum[lid + offset];
                    fractionSum[lid] = self + other;
                    int selfInt, otherInt;
                    selfInt = localMyNmutation[lid];
                    otherInt = localMyNmutation[lid + offset];
                    localMyNmutation[lid] = selfInt + otherInt;
                    selfInt = localMyNcross[lid];
                    otherInt = localMyNcross[lid + offset];
                    localMyNcross[lid] = selfInt + otherInt;
                }
                barrier(CLK_LOCAL_MEM_FENCE);
            }

            if (lid == 0)
            {
                localMax = fractionMax[0];
                localMin = fractionMin[0];
                localSumfitness = fractionSum[0];
                localAvg = localSumfitness / LOCAL_SIZE;
                localNmutation += localMyNmutation[0];
                localNcross += localMyNcross[0];
                if (localMax > localMaxPop.objective)
                {
                    localMaxPop.gen = *gen;
                    localMaxPop.objective = localMax;
                    for (j = 0; j < LOCAL_SIZE; j++)
                    {
                        if (localOldPop[j].objective == localMax)
                        {
                            pop_t maxpop = localOldPop[j];
                            localMaxPop.chrom[0] = maxpop.chrom[0];
                            localMaxPop.chrom[1] = maxpop.chrom[1];
                            localMaxPop.chrom[2] = maxpop.chrom[2];
                            localMaxPop.x[0] = maxpop.x[0];
                            localMaxPop.x[1] = maxpop.x[1];
                            localMaxPop.x[2] = maxpop.x[2];
                        }
                        maxPopBuffer[groupId] = localMaxPop;
                    }
                }
            }
            myOldPop = myNewPop;
            localOldPop[lid] = myOldPop;
            barrier(CLK_LOCAL_MEM_FENCE);
            if (gid == 0) *gen += 1;
        }

    }

    if (lid == 0)
    {
        maxBuffer[groupId] = localMax;
        minBuffer[groupId] = localMin;
        sumBuffer[groupId] = localSumfitness;
        nmutationBuffer[groupId] = localNmutation;
        ncrossBuffer[groupId] = localNcross;
    }
    barrier(CLK_GLOBAL_MEM_FENCE);
    globalOldPop[gid] = myOldPop;
    globalNewPop[gid] = myNewPop;
    if (gid == 0)
    {
        int maxIndex = 0;
        for (j = 0; j < globalSize / LOCAL_SIZE; j++)
        {
            if (*globalMax < maxBuffer[j])
            {
                *globalMax = maxBuffer[j];
                maxIndex = j;
            }
            *globalMin = min(*globalMin, maxBuffer[j]);
            *globalSumfitness += sumBuffer[j];
            *globalNmutation += nmutationBuffer[j];
            *globalNcross += ncrossBuffer[j];
        }
        *globalAvg = *globalSumfitness / (float) globalSize;
        *globalMaxPop = maxPopBuffer[maxIndex];
#ifdef DEBUG
        printf("chrom={%10d, %10d, %10d}, objective=%f, gen=%d\n", (*globalMaxPop).chrom[0], globalMaxPop->chrom[1], globalMaxPop->chrom[2], globalMaxPop->objective, globalMaxPop->gen);
#endif
    }
    storeRNGState(rngBuffer_R, rngBuffer_N, realR, &n, gid);
}