#ifndef SGA3_MAIN_HEADER
#define SGA3_MAIN_HEADER 0

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

#define Maxpop 512
#define Nvar   4
#define Maxchrom 16
#define Fmultiple 2
#define Tol      (pow((10.),(-30.)) )


#define FAIL_TO_FIND_DEVICE 1
#define FAIL_TO_CREATE_CONTEXT 2
#define FAIL_TO_CREATE_COMMAND_QUEUE 3
#define FAIL_TO_BUILD_PROGRAM 4
#define FAIL_TO_CREATE_KERNEL 5
#define FAIL_TO_CREATE_MEMORY_OBJECT 6
#define FAIL_TO_SET_KERNEL_ARGS 7
#define FAIL_TO_RUN_KERNEL 8
#define FAIL_TO_READ_BUFFER 9
#define FAIL_TO_OPEN_FILE 10
#define FAIL_TO_COPY_BUFFER 11

#define WHOLE_KERNEL_NAME "wholeKernel.cl"
#define WHOLE_KERNEL_FUNC "run"

#define LOCAL_WORK_SIZE 32

#define REPORTERR(x) if(x != CL_SUCCESS){printTextError(x);return x;}

typedef cl_int chromosome;

typedef struct {
    chromosome chrom[Nvar];
    cl_float x[Nvar], objective, fitness;
    cl_int parent1, parent2;
    cl_int xsite;
} ind_t;


typedef struct {
    cl_int gen;
    chromosome chrom[Nvar];
    cl_float x[Nvar];
    cl_float objective;
} max_t;

#include "sga3_cl_basic_func.h"
#include "sga3_report.h"

void initMemBuffers(cl_context context,
                    cl_int _popsize, cl_int _nvar, cl_float *_rvar, cl_int _lchrom, cl_int _gen,
                    cl_float _pmutation, cl_uint _nmutation, cl_uint _ncross,
                    cl_float _pcross, cl_int _seed, cl_int _maxgen,
                    cl_mem *popsize, cl_mem *oldpop, cl_mem *newpop,
                    cl_mem *ncross, cl_mem *pcross,
                    cl_mem *nmutation, cl_mem *pmutation,
                    cl_mem *nvar, cl_mem *rvar, cl_mem *lchrom,
                    cl_mem *min, cl_mem *max, cl_mem *avg, cl_mem *gen,
                    cl_mem *max_ult, cl_mem *sumfitness, cl_mem *seed,
                    cl_mem *nmutationBuffer, cl_mem *ncrossBuffer, cl_mem *maxBuffer,
                    cl_mem *minBuffer, cl_mem *sumBuffer, cl_mem *rngBuffer_R, cl_mem *rngBuffer_N,
                    cl_mem *maxPopBuffer, cl_mem *maxgen, cl_int *err);
void readMemBufferForReport(cl_command_queue commandQueue,
                            ind_t *_oldpop, ind_t *_newpop, cl_float *_max, cl_float *_avg,
                            cl_float *_min, cl_float *_sumfitness, cl_uint *_nmutation,
                            cl_uint *_ncross, max_t *_max_ult,
                            cl_mem *oldpop, cl_mem *newpop, cl_mem *max, cl_mem *avg,
                            cl_mem *min, cl_mem *sumfitness, cl_mem *nmutation,
                            cl_mem *ncross, cl_mem *max_ult,
                            cl_int _popsize, cl_int *err);

#endif
