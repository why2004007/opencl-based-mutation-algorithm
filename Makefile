CC=gcc
CFLAGS=-framework OpenCL
OBJECTS3 = sga3_cl.o sga3_cl_basic_func.o sga3_report.o

sga3 : $(OBJECTS3)
	gcc $(OBJECTS3) -lm -o sga3_cl -framework OpenCL
clean: 
	rm *.o
