#include "sga3_cl.h"

#ifndef REPORT_LIB
#define REPORT_LIB 0

void report(cl_int gen, ind_t *oldpop, ind_t *newpop,
            cl_int lchrom, cl_float max, cl_float avg, cl_float min, cl_float sumfitness,
            cl_uint nmutation, cl_uint ncross, cl_int popsize,
            cl_int nvar, max_t *max_ult);

void initreport(cl_int popsize, cl_int lchrom, cl_int maxgen, cl_float pcross,
                cl_float pmutation, cl_float max, cl_float avg, cl_float min,
                cl_float sumfitness);
#endif