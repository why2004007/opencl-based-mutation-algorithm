/*
 * The following code are GLIBC 
 * Random number generator.
 */

#ifndef RANDOM_GENERATOR
#define RANDOM_GENERATOR 0
void srand(uint *r, int *n, uint seed)
{
    r[0] = seed;
    for (int i = 1; i < 31; i++)
    {
        r[i] = (uint)((16807 * (ulong)r[i - 1]) % 2147483647);
    }
    for (int i = 31; i < 34; i++)
    {
        r[i] = r[i - 31];
    }
    for (int i = 34; i < 344; i++)
    {
        r[i] = r[i - 31] + r[i - 3];
    }
    *n = 0;
}

uint rand(uint *r, int *n)
{
    uint x = r[*n % 344] = r[(*n + 313) % 344] + r[(*n + 341) % 344];
    *n = (*n + 1) % 344;
    return x;
}
#endif