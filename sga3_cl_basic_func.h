#include "sga3_cl.h"
#ifndef SGA3_CL_BASIC_LIB
#define SGA3_CL_BASIC_LIB 0

void buildKernel(cl_context context, cl_kernel *kernel, char *fileName, char* functionName, cl_int *err);
void printTextError(cl_int err);
void initCLEnvironment(cl_context *context, cl_device_id *deviceIds,
                       cl_command_queue *commandQueue, cl_int *err);

#endif